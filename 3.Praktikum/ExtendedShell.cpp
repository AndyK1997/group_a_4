#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <vector>
#include <sys/wait.h>
#include <sstream>
using namespace std;

pid_t Pauseprozess; 								//Globale Variable für das Speichern der angehaltenen PID
pid_t Vordergrundprozess=getpid();							//Globale Variable für das Speichern der PID des zuletzt ausgeführten Prozesses
bool laeuft=false;								//Globaler Indikator ob ein Vordergrund-Prozess läuft
vector<pid_t>Hintergrundkinder;

int createProcess(char* program, char** arg_List, bool hintergrund){
    pid_t Kind_PID;
	Kind_PID=fork();							//Kind_PID=PID des Elternprozess

	if(Kind_PID != 0){
        if(hintergrund){					//wird ausgeführt wenn es sich um einen Hintergrundprozess handelt
			Hintergrundkinder.push_back(Kind_PID);			//ChildPID wird in den @BackgroundChilds-Vector geschrieben
		}
        else{                           //wird ausgeführt wenn es sich um eien Vordergrund Process handelt
            Vordergrundprozess=Kind_PID;					//currentPID=childPID
            laeuft=true;							//ProzessIndikator wird auf true gesetzt
        }
		return Kind_PID;
	}
    else{									//gibt es keinen Elternprozess wird ein neuer Elternprozess erstellt
		execvp(program, arg_List);
		fprintf(stderr, "Elternprozess-Erstellung fehlgeschlagen.\n");	// wird nur ausgegeben wenn ein Fehler auftritt
		abort();
	}
}

void INT_HANDLER(int signum){
    if((laeuft == false) && (Hintergrundkinder.empty() == true)){	//Wenn kein Vordergrundprozess läuft und es keinen HIntergrundprozess gibt
		cout << "Ich bin die Shell." << endl << "Beenden? [J(a)/N(ein)?]" << endl;
		int c;
		c=getchar();
		if(toupper(c) == 'J'){
			exit(EXIT_SUCCESS);				//Beendet die Shell ordungsgemäß
		}
		else{
			cin.sync();
			if(toupper(c) != 'N')
				cout << "Wie meinen, Meister?";
			else
				cout << "Wie lauten eure Befehle, Meister?" << endl;
		}
    }
    else if(Hintergrundkinder.empty() == false && laeuft == false){//Wenn kein Vordergrundprocess läuft und es Hintergrundprozesse gibt
		cout << endl << "Es laufen noch Hintergrundprozesse. Wollen Sie diese beeden und fortfahren [J(a)/N(ein)?]" << endl;
		int c;
		c=getchar();
		if(toupper(c) == 'J'){

//Iteriert über den Background Vector und sendet das Kill signal an alle PID um sie zu beenden.
		    for(int x=0; x < Hintergrundkinder.size(); x++){
		    	kill(Hintergrundkinder[x],SIGINT);	//Kill signal
	    	}

	    	exit(EXIT_SUCCESS);				//Beendet die Shell ordnungsgemäß
		}
	}else{							//wenn es Vordergrund prozesse gibt wird dieser beendet
		cout << "Ich bin ein Kind." << endl;
		laeuft=false;				//Indikator=false es läuft kein Vordergrund Prozess mehr
	}
}

void STP_HANDLER(int signum){
    Pauseprozess=Vordergrundprozess;
    kill(Vordergrundprozess,SIGTSTP);
}

//Setzt den angehaltenen Prozess fort
void weiterVordergrund(){
	laeuft=true;						//Indikator=true es läuft ein Vordergrund-Prozess
	kill(Pauseprozess, SIGCONT);				//Setzt den angehaltenen Prozess fort
	int status;						//Status für waitpid
	waitpid(Pauseprozess, &status, 0);			//Wartet bis der Vordergrund-Prozess beendet wurde
    cout << "Neuer Status: " << status << endl;
}

int main(){
    signal(SIGCHLD,SIG_IGN);					//Signal gegen Zombie-Prozesse
    signal(SIGINT,INT_HANDLER);					//Handler für Strg+C
    signal(SIGTSTP,STP_HANDLER);				//Handler für Strg+Z
    string Eingabe="";                          //String für Nutzereingabe
    vector<char*> Argv;                         //
    vector<string> Befehl;                      //
    bool alive=true;

    cout << "****************************\t My SHELL \t****************************" << endl;

    while (alive){
        bool undAngh=false;
        cout << "Wie lauten eure Befehle, Meister?" << endl;
        cin.sync();
        getline(cin, Eingabe);											//Befehl vom Benutzer einlesen
        cout << endl;
        if(Eingabe == "logout"){
            char confirm;
            cout << "Wollen Sie die Shell wirklich beenden? [J(a)/N(ein)?]" << endl;
            cin >> confirm;											//Eingabe als Character einlesen
            if(toupper(confirm) == 'J'){
                alive=false;
            }
            else{
                if(toupper(confirm) != 'N'){
                    cerr << "Ungueltige Eingabe. Setze fort..." << endl;
                }
                getline(cin, Eingabe);              //leere cin und Eingabestring
            }
        }
        else{
        if(Eingabe == "fg"){
            weiterVordergrund();
        }
        else if(Eingabe.back() == '&'){
            undAngh=true;
        }
        while(Eingabe.back()=='&' || Eingabe.back()==' ')
        Eingabe.pop_back();

        //Die Eingabe in die einzelnen Befehle aufteilen
        stringstream BUFFER(Eingabe);										//Buffer=Eingabe
        int cnt=0;												//Counter der Leerzeichen
        for(int x=0; x < Eingabe.size(); x++){									//zählt die Leerzeichen
            if(Eingabe[x] == ' ')
		    cnt++;
        }
        for(int x=0; x <= cnt; x++){
            string tmp;
            getline(BUFFER,tmp,' ');									//liest von Leerzeichen zu Leerzeichen
            Befehl.push_back(tmp);										//speichert den Befehls-Substring
            Argv.push_back(const_cast<char*>(Befehl.back().c_str())); 					//speichert die Referenz auf den Befehls-Substring
            cout << x << ' ' << Argv.back() << endl;							//gibt den Substring zur Kontrolle aus
        }
        Argv.push_back(NULL);											//hängt Null am Ende des Befehls an als Abschlusszeichen, siehe Konvention

        if(!undAngh){										//wird im Falle eines Vordergrundprozesses aufgerufen
            char** commandptr=Argv.data();									//commandptr = Referenz des Argv Vectors
            pid_t PID=createProcess(Argv[0], commandptr, false);							//PID ist die Fork Funktions-PID falls es schon einen Elternprozess gibt
            int child_status;
            waitpid(PID,&child_status, WUNTRACED|WCONTINUED);						//wartet darauf dass der Prozess beendet wurde
            if(WIFEXITED (child_status)){
                printf("Der Kindprozess wurde mit dem Status %d beendet.\n",WEXITSTATUS(child_status));
            }
            else{
                printf("Der Kindprozess wurde unerwarted beendet.\n");
            }
            laeuft=false;
        }
        else {										//wird im Falle eines Hintergrundprozesses aufgerufen
        char** commandptr=Argv.data();									//commandptr = Referenz des Argv Vectors
        createProcess(Argv[0], commandptr, true);								//ruft die Fork-Funktion auf
        printf("Hintergrundprozess ausgeführt\n");
        }}
        Argv.clear();												//löscht den Referenz-Vector
        Befehl.clear();												//löscht den String-Vector
        cout << endl;
    }
    return 0;
}