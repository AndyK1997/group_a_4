#include <iostream>
#include <string>
#include <unistd.h>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sstream>
#include <cstdlib>
#include <fcntl.h>
using namespace std;

pid_t SleepingProcess; 						//Globale Variable für das speichern der Angehaltenen PID
pid_t currentProcess=getpid();				//Globale Variable für das Speichern der PID des Zuletzt Ausgeführten Prozesses
bool isRunning=false;						//Globaler Indikator ob ein Vordergrund Prozess Läuft 
vector<pid_t> BackgroundChilds;				//Globaler Vector der alle PID der Hintergrund Prozesse enthält 
char PROCESS_TYPE='F';						//GlobaleVariable zum Unterscheiden der einzelnen Prozesstypen (F,B,T)


//Neuen Process erzeugen (Forken) der Kind Process erbt die PID des Eltern Process ist die PID =0 ist es selbst der Eltern Process!!
int spawnNewProcess(char* program, char** arg_List){
	pid_t child_pid;
	child_pid=fork();									//child_pid=PID des Eltern Process
	
	//ermitteln ob es einen Eltern Prozess gibt
	if(child_pid!=0){
		if(PROCESS_TYPE=='F'){							//Wird ausgeführt wenn es sich um eien Vordergrund Process handelt 
		currentProcess=child_pid;						//currentPID=childPID
		isRunning=true;									//ProzessIndikator wird auf true gesetzt 
		}else if(PROCESS_TYPE=='B'){					//wird ausgeführt wenn es sich um eien Hintergrund Prozess handelt 
			BackgroundChilds.push_back(child_pid);		//ChildPID wird in den @BackgroundChilds vector geschrieben 
		}
		return child_pid;
	}else{											//Gibt es keinen Eltern Process wird ein neuer Eltern Prozess erstellt 
		execvp(program, arg_List);
		fprintf(stderr, "Erstellung Parent Fehlgeschlagen\n"); 	// wird nir ausgegeben wenn ein Fehler auftritt
		abort();
	}
}


//Signal handeler für SIGINIT, Programm beenden , Process Beenden  
void MY_SIG_HANDLER(int signum){

	if(isRunning==false&&BackgroundChilds.empty()==true){	//Wenn kein Vordergrund process läuft und es keinen HInterfrundprocesse gibt
		cout<<"Iam your shell"<<endl;
		cout<<"Beenden j=ja|n=Nein"<<endl;
		int c;
		c=getchar();
		if(c=='j'){				
			exit(EXIT_SUCCESS);								//Beendet die Shell ordungsgemäß
		}
		else{
			cin.sync();
		    cout <<"Wie lauten euere Befehle Meister"<<endl;	
		}
	}else if(BackgroundChilds.empty()==false && isRunning==false){//Wenn kein Vordergrundprocess läuft und es Hintergrundprozesse gibt
		cout<<"\nes giebt noch laufende hintergrund prozessen wollen sie diese beeden und fortfahren j=ja"<<endl;
		int c; 
		c=getchar();
		if(c=='j'){

//Iteriert über den Background Vector und sendet das Kill signal an alle PID um sie zu beenden 			
		for(int x=0; x<BackgroundChilds.size(); x++){
			kill(BackgroundChilds[x],SIGINT);				//Kill signal
		}

		exit(EXIT_SUCCESS);									//Beendet die Shell Ordnungsgemäß
		}
		else{
//Zurück zum programm wenn die Eingabe !=j ist 
		}
	}else										//wenn es Vordergrund prozesse gibt wird dieser beendet 
	{
		cout<<"IAM a Child"<<endl;
		isRunning=false;						//Indikator=false es läuft kein Vordergrund Process mehr 
	}
	
	
}
//Signal handler für SIGTSTP hält den Process an und speichert die PID 
void MY_SIG_HANDLER2(int signum){
	SleepingProcess=currentProcess;				//hier wird die PID gespeichert 
    kill(currentProcess,SIGTSTP);				//Hällt den aktuellen Process an 
	isRunning =false;							//Indikator=false es läuft kein Vordergrund Process mehr 
}

//Setzt den angehaltenen Process fort 
void ContinueForeground(){
	isRunning=true;								//Indikator=true es läuft ein Vordergrund Process
	kill(SleepingProcess, SIGCONT);				//Setzt den angehaltenen Process fort 
	int status;									//status für waitpid
	waitpid(SleepingProcess,&status, 0);		//Wartet bis der Vordergrund Process beendet wurde 
}

//Setzt den angehaltenen prozess im Hintergrund fort 
void ContinueBackground(){					
	kill(SleepingProcess,SIGCONT);				//Setzt den angehaltenen Process fort 
	BackgroundChilds.push_back(SleepingProcess);//Speichert die PID im Background Vector 
}

int main()
{
signal(SIGCHLD,SIG_IGN);						//Signal gegen Zombie Prozesse 
signal(SIGINT,MY_SIG_HANDLER);					//Signal für Interrupts strg+c
signal(SIGTSTP,MY_SIG_HANDLER2);				//Signal zum anhalten strg+z
string input="leer";
vector<char*> Argv;
vector<string> Befehl;


std::cout << "****************************\t My SHELL \t****************************" << std::endl;
bool PROGAN=true;
while (PROGAN) {
PROCESS_TYPE='F';
cout <<"Wie lauten euere Befehle Meister"<<endl;
cin.sync();
getline(cin,input);										//Befehl vom Benutzer einlesen 
if(input=="logout"){

	char confirm ;
	cout<<"Wollen sie wirklich beenden'Y/N"<<endl;
	cin>>confirm;
	if(toupper(confirm)=='Y'){
		PROGAN=false;
		break;
	}else{
		continue;
	}
}
else if(input[input.size()-1]=='&'){					//Löscht das & Zeichen und Setzt den TYP auf Bacḱground
	input.pop_back();									//Löscht &
	PROCESS_TYPE='B';									//TYP = Background (B) 
	
}
else if(input=="fg"||input=="bg"){
	if(input=="fg"){
		PROCESS_TYPE='T';
		ContinueForeground();
	}
	else if(input=="bg"){
		PROCESS_TYPE='T';
		ContinueBackground();
	}
	
	
}

//Die Eingabe in die einzelnen Befehle aufteilen 
stringstream BUFFER(input);								//Buffer=input
int cnt=0;												//Counter der Leerzeichen 
for(int x=0; x<input.size(); x++){						//Zählt die Leerzeichen
	if(input[x]==' '){
		cnt++;
	}
}
for(int x=0; x<cnt+1; x++){
	string tmp;
	getline(BUFFER,tmp,' ');							//Liest von Leerzeichen zu Leerzeichen 
	Befehl.push_back(tmp);								//Speichert den Befehls substring 
	Argv.push_back(const_cast<char*>(Befehl.back().c_str())); 		//Speichert die Referenz auf den Befehls substring 
	cout<<x<<Argv.back()<<endl;							//gibt den Substring zur Kontrolle aus 
}
Argv.push_back(NULL);									//Hängt Null am ende vom Befehl an als Abschlusszeichen siehe Konvention 


if(PROCESS_TYPE=='F'){									//Wird im Falle eines Vordergrund Prozesses aufgerufen 
	char** commandptr=Argv.data();						//commandptr = Referenz des Argv Vectors
	pid_t PID=spawnNewProcess(Argv[0], commandptr);		//PID ist die Fork funktions PID falls es schon eien Eltern prozess gibt 
	
	int child_status;
	waitpid(PID,&child_status, WUNTRACED|WCONTINUED);						//Wartet darauf das der Prozess beendet wurde 

	if(WIFEXITED (child_status)){
		printf("Der Kind Prozess wurde mit dem Status %d beendet\n",WEXITSTATUS(child_status));
		isRunning=false;
	
	}else{
		printf("Der Kind Prozess wurde unerwarted beendet\n");
		isRunning=false;
	}
}
else if(PROCESS_TYPE=='B'){								//Wird im Falle eines Hintergrundprozesses aufgerufen 
	char** commandptr=Argv.data();						//commandptr = Referenz des Argv Vectors
	spawnNewProcess(Argv[0], commandptr);				//Ruft die Fork funktion auf 
	printf("Background Process Ausgeführt\n");
}
Argv.clear();											//Löscht den Referenz Vector 
Befehl.clear();											//Löscht den String Vector
}
return 0;
}

