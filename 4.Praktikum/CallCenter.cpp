#include<iostream>
#include<cstdlib>
#include<string>
#include<cmath>
#include<ctime>
#include<thread>
#include<unistd.h>
#include<queue>
#include<mutex>
#include<chrono>
#include<condition_variable>
#include<pthread.h>

static int callerID=0;                  //Global Anrufer ID um die einzelene Anrufer unterscheiden zu können 
int CountOfCaller=0;                    //Global Anzahl der Anrufer die vom Benutzer eingelesen wird
int QueueSize=0;                        //Global Queue Größe die von dem Benutzer eingelesen wird 

std::mutex WarteschlangenMutex;                     //Mutex für die Sperrung der Warteschlange
std::mutex Queue;                       //Mutex für die Sperrung der angenommenen Anrufs Vector 
std::condition_variable myCondition;    //condition_Variable für die Warteschlange
std::condition_variable myCondition2;   //condition_Variable für den Angenommenen Anrufs Vector 


//Meine Datenstruktur für den Anrufer Name=Anrufer+ID callDuration = rand()Zahl für die Anrufdauer
struct Caller{                              
    std::string Name;
    int callDuration;
};

//Meine Globalen Vectoren 
std::queue<Caller*> Warteschlange;
std::vector<Caller*> Angenommen;

//Funktion zum ermitteln ob der Anruf angenomen wurde
bool WarteschlangenScan(std::string Name){
    /*std::unique_lock<std::mutex> Qlock(Queue);
    myCondition2.wait(Qlock,[], {return !Angenommen.empty();});
    for(int x=0; x<Angenommen.size();x++){
        if(Angenommen[x]->Name==Name){
            Angenommen.erase(Angenommen.begin()+ x);
            Qlock.unlock();
            return true;
        }
    
    }*/
    for(int x=0; x<Angenommen.size(); x++){
        if(Angenommen[x]->Name==Name){
            std::scoped_lock<std::mutex> Qlock(Queue);
            Angenommen.erase(Angenommen.begin()+x);
            return true;
        }
    }
    return false;
}


//
//Thread Funktion die einen Anrufer simmuliert 
//
void Call(){

    Caller *newCaller =new Caller;
    srand((unsigned)time(0));
    newCaller->Name="Anrufer"+std::to_string(callerID);
    newCaller->callDuration=rand()%5;
    callerID++;

    while(true){
        //std::unique_lock<std::mutex> lock(WarteschlangenMutex); 
       
        if(Warteschlange.size()<QueueSize){
            std::scoped_lock<std::mutex> lock(WarteschlangenMutex);
            Warteschlange.push(newCaller);
            
            //lock.unlock();
            myCondition.notify_one();
            break;
        }
        else{
            //lock.unlock();
            sleep(2); 
        }
    }
    while (true){ 
       if(WarteschlangenScan(newCaller->Name)==false){
           continue;
       }else{
            std::cout<<newCaller->Name<<" hat aufgelegt\n";
           break;
       }
    }
}


//
//Thread funktion die einen Mitarbeiter simmuliert 
//
void Employee(int x){
    std::cout<<"Mitarbeiter "<< x<<" wartet auf Kunden\n";
    while (true){
    Caller *tmp;
    bool onSpeak=false;
    std::unique_lock<std::mutex> lock(WarteschlangenMutex);
    myCondition.wait(lock,[]{return !Warteschlange.empty();});
    
    if(!Warteschlange.empty()){
    tmp=Warteschlange.front();
    Warteschlange.pop();
    lock.unlock();
    onSpeak=true;
    }

    if(onSpeak==true){
        std::cout<<"Gespräch von Mitarbeiter"<< x <<" mit"<<tmp->Name<<" angenommen\n";
        sleep(tmp->callDuration);
        std::unique_lock<std::mutex> Qlock(Queue);
        Angenommen.push_back(tmp);
        Qlock.unlock();
        //myCondition2.notify_all();
    }

    if(Warteschlange.empty()&&callerID==CountOfCaller){
        myCondition2.notify_all();
        break;
    }

    }
}



int main(){

    std::cout<<"Callcenter Fun Test system"<<std::endl<<std::endl;
    int Eingabe =0;

    //Mitarbeiter Anzahl einlesen 
    std::cout<<"Anzahl Mitarbeiter: ";
    std::cin>>Eingabe;
    const int CountOfEmployee=Eingabe;

    //Anrufer Anzahl einlesen
    std::cout<<"Anzahl der Anrufer: ";
    std::cin>>Eingabe;
    CountOfCaller=Eingabe;

    //Größe der Warteschlange einlesen 
    std::cout<<"Groeße der Warteschlange: ";
    std::cin>>Eingabe;
    QueueSize=Eingabe;
    

    std::vector<std::thread*> CallerList;           //Vector für die einzelen Anrufer erzeugen 
    std::vector<std::thread*> allEmployees;         //Vector für die einzelnen Mitarbeiter erzeugen 

    //Alle Mitarbeiter Threads starten ==Fangen an zu arbeiten 
    for(unsigned int x=0; x<CountOfEmployee; x++){
        
        allEmployees.push_back( new  std::thread(Employee,x));
    }

    //Alle Anrufer Threads starten ==Kunden rufen an 
    for(unsigned int x=0; x<CountOfCaller; x++){
        CallerList.push_back(new std::thread(Call));
    }
    
    for(unsigned int x=0; x<CountOfEmployee; x++){
        allEmployees[x]->join();
    }
    
    for(unsigned int x=0; x<CountOfCaller; x++){
        CallerList[x]->join();
    }
    
    return 0;
}