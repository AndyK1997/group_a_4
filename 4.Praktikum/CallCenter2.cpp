//#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <queue>
#include <ctime>
#include <unistd.h>
#include <pthread.h>
#include <mutex>
#include <chrono>
#include <condition_variable>

unsigned int anrId=0, mA=3, lWs=15, anrAz=100;	//Standard-AnruferId=0 und Standardwerte für Mengen

std::mutex queueM;				//Mutex für Warteschlange/Queue
std::condition_variable cond;	//Semaphoren-Bedingung

struct Anrufer{
	unsigned int id=0/*anrId++*/, dauer=1+rand()%5;	//Anrufer-Struktur wird initialisiert
	bool aktiv=true;								//Anrufer wird aktiv gesetzt
};

std::queue<Anrufer*>warteschlange;	//Warteschlange für Anrufer

class Mitarbeiter{
	public:
		Mitarbeiter(){};
		Mitarbeiter(unsigned int i){id=i;};
		void beantworten(unsigned int threadid){
			if(id == threadid)
			printf("Mitarbeiter %d meldet sich zum Dienst.\n", id+1);
			else{
				printf("Das bin ich nicht!\n");
			}
			while(anrAz>0){					//solange nicht alle Anrufer abgeholt wurden
				if(anruferHolen()){			//falls ein Anrufer durchgestellt werden kann
					printf("Mitarbeiter %d ist nun im Gespräch mit Anrufer %d für %d Sekunden.\n", id+1, kunde->id+1, kunde->dauer);
					sleep(kunde->dauer);	//Anrufer simulieren durch vordefiniertes Warten
					kunde->aktiv=false;		//Anrufer wird inaktiv gesetzt
					//cond.notify_all();
					printf("Mitarbeiter %d hat das Gespräch mit Anrufer %d beendet.\n", id+1, kunde->id+1);
					kunde=nullptr;			//calling customer no longer connected
				}
			}
		}
	private:
		unsigned int id=0;		//Prototypen-Objekt auf 0 initialisieren
		Anrufer* kunde=nullptr;	//Nullzeiger = kein verbundener Anrufer zu Beginn
		bool anruferHolen(){							//Funktion zum Aufsammeln von Anrufern aus der Warteschlange
			std::unique_lock<std::mutex> lock(queueM);	//Warteschlange sperren zum Aufsammeln von Anrufern...
			if(!warteschlange.empty()){					//...falls mindestens einer verbleibt
				kunde=warteschlange.front();			//verbinde den Anrufer zu diesem Mitarbeiter
				warteschlange.pop();					//entferne den Anrufer aus der Wartschlange
				anrAz--;								//verringere die Anzahl verbleibender Anrufer
				lock.unlock();							//Warteschlange für Modifikation entsperren
				return true;							//gib true zurück bei erfolgreichem Durchstellen
			}
			lock.unlock();								//Warteschlange für Modifikation entsperren
			cond.wait(lock,[]{return !warteschlange.empty();});
			anruferHolen();
			return false;								//gib false zurück bei fehlgeschlagenem Durchstellen
		}
};

//std::vector<Anrufer>uAnrufer;			//unbenutzer Vektor um Anrufer zu verwalten
std::vector<Mitarbeiter>uMitarbeiter;	//Vektor um Mitarbeiter zu verwalten

void *beantworte(void *threadid){		//initiale Mitarbeiter-Thread-Funktion
	unsigned int id = (long)threadid;	//übernimm die übergebene ThreadId
	uMitarbeiter[id].beantworten(id);	//lasse den zugehörigen Mitarbeiter die Arbeit aufnehmen
	pthread_exit(NULL);					//bei Vollendung verlasse Thread
}

void *anrufen(void *threadid){
	Anrufer *ich = new Anrufer();					//neuen Anrufer für Thread erzeugen, Id post-inkrementieren; Alternative: &uAnrufer[(long)id];
	ich->id=(long)threadid;							//manuell die ThreadId als AnruferId nutzen
	while(true){									//Anrufer versucht bis Erfolg die Warteschlange zu betreten
		std::unique_lock<std::mutex> lock(queueM);	//Warteschlange sperren zum Hinzufügen von Anrufern...
		if(warteschlange.size() < lWs){				//prüfe ob Warteschlange voll
			warteschlange.push(ich);				//füge Anrufer zur Warteschlange hinzu
			lock.unlock();							//Warteschlange für Modifikation entsperren
			printf("Anrufer %d ist nun in der Warteschlange mit Größe %d.\n",ich->id+1,warteschlange.size());
			break;									//Warteschlange erreicht, Abbruch
		}
		else{
			lock.unlock();
			sleep(1);
		}

	}
	while(ich->aktiv==true);
	printf("Anrufer %d hat aufgelegt.\n",(long)threadid+1);
	pthread_exit(NULL);
}

int main(int argc, char* argv[]){
	int Eingabe;

	//diverses Einlesen von Werten und Initialisierung der Container
	std::cout << "Anzahl Mitarbeiter: ";
	std::cin >> Eingabe;
	if(Eingabe > 0)
	mA = Eingabe;
	for(size_t i=0; i < mA; i++)
	uMitarbeiter.push_back(Mitarbeiter(i));
	
	std::cout << "Länge Warteschlange: ";
	std::cin >> Eingabe;
	if(Eingabe > 0)
	lWs = Eingabe;

	std::cout << "Anzahl Anrufer: ";
	std::cin >> Eingabe;
	if(Eingabe > 0)
	anrAz = Eingabe;
	/*for(size_t i=0; i < anrAz; i++)
	uAnrufer.push_back(Anrufer());*/

	int rc;
	srand(time(NULL));	//erhöhe Zufälligkeit der Zufallszahlen
	pthread_t anrT[anrAz];	//C-Array für Posix-Threads der Anrufer anlegen
	for(size_t i=0; i < anrAz; i++){//Anrufer rufen an
		rc = pthread_create(&anrT[i], NULL, anrufen, reinterpret_cast<void *>(i));
		
		if (rc) {
			std::cerr << "Anrufer " << i+1 << ": Telefon verloren oder kaputt." << std::endl;
			exit(-1);
		}
	}

	pthread_t maT[mA];		//C-Array für Posix-Threads der Mitarbeiter anlegen
	for(size_t i=0; i < mA; i++){	//Mitarbeiter beginnen den Dienst
		rc = pthread_create(&maT[i], NULL, beantworte, reinterpret_cast<void *>(i));
		
		if (rc) {
			std::cerr << "Mitarbeiter " << i+1 << ": Fehler beim Arbeitsbeginn." << std::endl;
			exit(-1);
		}
	}

	for(size_t i=0; i < anrAz; i++)
	pthread_join(anrT[i],NULL);		//warte bis alle Anrufer abgearbeitet wurden

	for(size_t i=0; i < mA; i++)
	pthread_join(maT[i],NULL);		//warte bis alle Mitarbeiter-Threads beendet wurden

	return 0;
}