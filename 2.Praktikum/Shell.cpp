#include <iostream>
#include <string>
#include <unistd.h>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sstream>
#include <cstdlib>
#include <fcntl.h>
using namespace std;

//Neuen Process erzeugen (Forken) der Kind Process erbt die PID des Eltern Process ist die PID =0 ist es selbst der Eltern Process!!
int spawnNewProcess(char* program, char** arg_List){
	pid_t child_pid;
	child_pid=fork();									//child_pid=PID des Eltern Process
	
	//ermitteln ob es einen Eltern Prozess gibt
	if(child_pid!=0){
		return child_pid;
	}else{  											//Gibt es keinen Eltern Process wird ein neuer Eltern Prozess erstellt 
		execvp(program, arg_List);
		fprintf(stderr, "Erstellung Parent Fehlgeschlagen\n"); 	// wird nir ausgegeben wenn ein Fehler auftritt
		abort();
	}
}

int main()
{
string input;
vector<char*> Argv;
vector<string> Befehl;
bool PROGAN=true;
char PROCESS_TYPE='F';
std::cout << "****************************\t My SHELL \t****************************" << std::endl;
while (PROGAN) {
PROCESS_TYPE='F';
cout <<"Wie lauten euere Befehle Meister"<<endl;
getline(cin,input);										//Befehl vom Benutzer einlesen 
if(input=="logout"){

	char confirm ;
	cout<<"Wollen sie wirklich beenden'Y/N"<<endl;
	cin>>confirm;
	if(toupper(confirm)=='Y'){
		PROGAN=false;
		break;
	}else{
		continue;
	}
}
else if(input[input.size()-1]=='&'){					//Löscht das & Zeichen und Setzt den TYP auf Bacḱground
	input.pop_back();									//Löscht &
	PROCESS_TYPE='B';									//TYP = Background (B) 
	
}


//Die Eingabe in die einzelnen Befehle aufteilen 
stringstream BUFFER(input);								//Buffer=input
int cnt=0;												//Counter der Leerzeichen 
for(int x=0; x<input.size(); x++){						//Zählt die Leerzeichen
	if(input[x]==' '){
		cnt++;
	}
}
for(int x=0; x<cnt+1; x++){
	string tmp;
	getline(BUFFER,tmp,' ');							//Liest von Leerzeichen zu Leerzeichen 
	Befehl.push_back(tmp);								//Speichert den Befehls substring 
	Argv.push_back(const_cast<char*>(Befehl.back().c_str())); 		//Speichert die Referenz auf den Befehls substring 
	cout<<x<<Argv.back()<<endl;							//gibt den Substring zur Kontrolle aus 
}
Argv.push_back(NULL);									//Hängt Null am ende vom Befehl an als Abschlusszeichen siehe Konvention 


if(PROCESS_TYPE=='F'){									//Wird im Falle eines Vordergrund Prozesses aufgerufen 
	char** commandptr=Argv.data();						//commandptr = Referenz des Argv Vectors
	pid_t PID=spawnNewProcess(Argv[0], commandptr);		//PID ist die Fork funktions PID falls es schon eien Eltern prozess gibt 
	
	int child_status;
	waitpid(PID,&child_status, 0);						//Wartet darauf das der Prozess beendet wurde 

	if(WIFEXITED (child_status)){
		printf("Der Kind Prozess wurde mit dem Status %d beendet\n",WEXITSTATUS(child_status));
	
	}else{
		printf("Der Kind Prozess wurde unerwarted beendet\n");
	}
}
else if(PROCESS_TYPE=='B'){								//Wird im Falle eines Hintergrundprozesses aufgerufen 
	char** commandptr=Argv.data();						//commandptr = Referenz des Argv Vectors
	spawnNewProcess(Argv[0], commandptr);				//Ruft die Fork funktion auf 
	printf("Background Process Ausgeführt\n");
}
Argv.clear();											//Löscht den Referenz Vector 
Befehl.clear();											//Löscht den String Vector
}
return 0;
}

