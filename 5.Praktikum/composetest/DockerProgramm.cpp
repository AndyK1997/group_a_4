#include <iostream>
#include <cstdlib>
#include <thread>
#include <vector>

void Funktion(){
    int *Array;
    int *Array2;
    const int limes =1000;
    Array= new int[limes];
    Array2= new int[limes];
    int Wert=1;
    while(true){
        for(unsigned int x=0; x<10; x++){
            Array[x]=Wert;
            Array2[x]=Wert;
        }
        Wert++;
        if(Wert>10000000){
            Wert=0;
        }
    }
}

int main(){
    std::vector<std::thread*> AllThreads;
    const int MaxThreads=200;
    for(unsigned int x=0; x<MaxThreads; x++){
        AllThreads.push_back(new std::thread(Funktion));
    }
    for(unsigned int x=0; x<MaxThreads; x++){
        AllThreads[x]->join();
    }
    exit(EXIT_SUCCESS);
}