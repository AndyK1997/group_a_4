#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <vector>
#include <time.h>

#define THREADC 42
#define VECTORS 100000000 //memory usage ~*=8 Bytes

using namespace std;

vector <long>zahlen;

void *printz(void *startpoint){
    for(long i=reinterpret_cast<long>(startpoint); i < (reinterpret_cast<long>(startpoint) + (VECTORS/THREADC)); i++)
    printf("%ld",zahlen.at(i));
    //zahlen.at(i)++;
    pthread_exit(NULL);
}

int main(int argc, char* argv[]){
    
    for(size_t i=0; i<VECTORS; i++){
        zahlen.push_back(rand());
    }

    pthread_t workT[THREADC];
    for(size_t i=0; i<THREADC; i++){
        pthread_create(&workT[i], NULL, printz, reinterpret_cast<void *>((i)*VECTORS/THREADC));
    }

    for(size_t i=0; i < THREADC; i++)
	pthread_join(workT[i],NULL);

    return 0;
}